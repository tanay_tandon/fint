# Fint

Fint is an Android library that interacts with the [IEX Trading REST API](https://iextrading.com/developer/)
and makes the IEX API data available in an easy way.

# How to use

`git clone git@bitbucket.org:tanay_tandon/fint.git`

Import the project Fint as a library in your project. If you are using Android studio
***Goto File -> New -> Import Module -> Path to Fint***.

Check your Projects ***settings.gradle*** file.
It should be like
`include ':app', ':fint'`

In your app module's ***build.gradle*** add the line
`implementation project(':fint')`

Sync your gradle files and you should be able to access [Fint](https://bitbucket.org/tanay_tandon/fint/src/master/src/main/java/com/example/tanaytandon/fint/internal/Fint.java)
in your app module.

    Fint.with(ApplicationContext).search(symbol, new FintCallback<CInfo>() {
             @Override
             public void onSuccess(CInfo result) {
                // called when company exists
             }

             @Override
             public void onFail(NetworkError networkError) {
                // called when company does not exist
             });
