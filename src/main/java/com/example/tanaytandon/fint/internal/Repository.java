package com.example.tanaytandon.fint.internal;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.tanaytandon.fint.FintCallback;
import com.example.tanaytandon.fint.models.CInfo;
import com.example.tanaytandon.fint.models.NetworkError;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Responsible for fetching all the information from the API via the {@link NetworkManager} class.
 */
class Repository {

    private static Repository instance;

    private NetworkManager networkManager;

    private Repository(Context context) {
        this.networkManager = NetworkManager.newInstance(context.getApplicationContext());
    }

    static synchronized Repository newInstance(Context context) {
        if (instance == null) {
            instance = new Repository(context);
        }
        return instance;
    }


    int search(String text, final FintCallback<CInfo> callback) {
        return networkManager.search(text, getListener(callback));
    }

    /**
     * wraps the {@link FintCallback} inside a {@link NetworkInteractionListener}
     * @param callback non nullable instance passed to the class
     */
    private <T> NetworkInteractionListener<T> getListener(final FintCallback<T> callback) {
        return new NetworkInteractionListener<T>() {
            @Override
            public void onSuccess(T result) {
                callback.onSuccess(result);
            }

            @Override
            public void onFail(NetworkError networkError) {
                callback.onFail(networkError);
            }
        };
    }

}
