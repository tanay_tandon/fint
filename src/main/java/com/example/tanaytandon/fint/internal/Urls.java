package com.example.tanaytandon.fint.internal;

interface Urls {
    String BASE_URL = "https://api.iextrading.com";

    String SEARCH = BASE_URL + "/1.0/stock/%s/batch";
}
