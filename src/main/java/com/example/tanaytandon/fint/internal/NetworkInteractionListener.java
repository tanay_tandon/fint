package com.example.tanaytandon.fint.internal;

import com.example.tanaytandon.fint.models.NetworkError;

public interface NetworkInteractionListener<T> {

    void onSuccess(T result);

    void onFail(NetworkError networkError);
}
