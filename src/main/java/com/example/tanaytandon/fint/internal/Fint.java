package com.example.tanaytandon.fint.internal;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.example.tanaytandon.fint.FintCallback;
import com.example.tanaytandon.fint.models.CInfo;

/**
 * Use an instance of this class to interact with the library.
 * Create an instance using the {@link Fint#with(Context)} method.
 * Only public class in the library, calls the {@link Repository} class
 * to fetch the relevant information.
 */

public class Fint {

    private Fint(Context context) {
        this.context = context.getApplicationContext();
        this.repository = Repository.newInstance(context.getApplicationContext());
    }

    public static final String TAG = "Fint";


    private Context context;

    private Repository repository;

    private static volatile Fint singleton;

    /**
     * Use this method to get an instance.
     *
     * @param context The Application Context
     * @return instance of the class
     */
    public static Fint with(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null");
        }
        if (singleton == null) {
            synchronized (Fint.class) {
                singleton = new Fint(context);
            }
        }
        return singleton;
    }

    /**
     * Looks up a company information based on their symbol
     *
     * @param symbol   non nullable company symbol
     * @param callback non nullable callback
     * @return id denoting the request, using which it can be cancelled.
     */
    public int search(String symbol, FintCallback<CInfo> callback) {
        if (symbol == null) {
            throw new IllegalArgumentException("Text cannot be null");
        } else if (callback == null) {
            throw new IllegalArgumentException("Callback cannot be null");
        } else if (ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            throw new IllegalStateException("Must have internet permission");
        }

        return repository.search(symbol, callback);
    }


}
