package com.example.tanaytandon.fint.internal;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.tanaytandon.fint.models.NetworkError;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

class ResponseHelper<T> {

    private static final String TAG = "ResponseHelper";

    /**
     * returns a POJO from the {@link NetworkResponse} via GSON.
     */
    static synchronized <T> Response<T> parseModel(NetworkResponse networkResponse, Type type) {
        try {
            Gson gson = new Gson();
            String json = jsonFromResponse(networkResponse);
            T model = gson.fromJson(json, type);
            return Response.success(model, HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.error(new ParseError(ex));
        }
    }

    private static String jsonFromResponse(NetworkResponse networkResponse) throws UnsupportedEncodingException,
            NullPointerException {
        return new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
    }

    static synchronized NetworkError getNetworkErrorFromVolleyError(VolleyError volleyError) {
        String message = "Oops unexpected error";
        int statusCode = 0;

        if (volleyError != null) {
            try {
                message = jsonFromResponse(volleyError.networkResponse);
                statusCode = volleyError.networkResponse.statusCode;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


        return NetworkError.newInstance(message, statusCode);
    }
}
