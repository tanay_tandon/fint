package com.example.tanaytandon.fint.internal;

import java.util.Collections;
import java.util.Map;

class SearchRequest extends NetworkRequestWrapper {

    private String text;

    public SearchRequest(String text) {
        this.text = text;
    }

    @Override
    String getURL() {
        return String.format(Urls.SEARCH, text) + "?types=company,price";
    }

    @Override
    Map<String, String> getHeaders() {
        return Collections.emptyMap();
    }

    @Override
    String getBody() {
        return null;
    }

}
