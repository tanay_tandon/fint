package com.example.tanaytandon.fint.internal;

import android.content.Context;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.tanaytandon.fint.models.CInfo;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Responsible for actually fetching the data from the API using Volley.
 * There are two approaches for making a network request.
 * Either define
 * a request class e.g {@link SearchRequest} and call the corresponding method
 * e.g for GET call {@link NetworkRequestWrapper#get(NetworkInteractionListener, Class, boolean, Object)}
 * for POST call {@link NetworkRequestWrapper#post(NetworkInteractionListener, Class, boolean, Object)}
 * or
 * call the methods {@link NetworkManager#get(String, Map, NetworkInteractionListener, Class, boolean, Object)}
 * {@link NetworkManager#post(String, String, Map, NetworkInteractionListener, Class, boolean, Object)}
 * The first method results in additional classes but those classes are easier to test.
 */

class NetworkManager {

    private static NetworkManager instance;
    private RequestQueue requestQueue;
    private AtomicInteger atomicInteger;

    private NetworkManager(Context context) {
        this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        this.atomicInteger = new AtomicInteger(10);
    }

    synchronized static NetworkManager newInstance(Context context) {

        if (instance == null) {
            instance = new NetworkManager(context.getApplicationContext());
        }

        return instance;
    }

    private RequestQueue getRequestQueue() {
        return requestQueue;
    }

    private void addToRequestQueue(Request request) {
        getRequestQueue().add(request);
    }

    void cancelRequest(Object tag) {
        getRequestQueue().cancelAll(tag);
    }

    int search(String symbol, NetworkInteractionListener<CInfo> networkInteractionListener) {
        int tag = atomicInteger.incrementAndGet();
//        String url = String.format(Urls.SEARCH, symbol) + "?types=company,price";
//        addToRequestQueue(get(url, Collections.<String, String>emptyMap(),
//                networkInteractionListener, CInfo.class, false, tag));
        SearchRequest request = new SearchRequest(symbol);
        addToRequestQueue(request.get(networkInteractionListener, CInfo.class, false, tag));
        return tag;
    }

    private <T> Request<T> get(String url, Map<String, String> headers, final NetworkInteractionListener<T> networkInteractionListener,
                               Class<T> clazz, boolean isResponseList, Object tag) {
        return makeRequest(url, null, headers, Request.Method.GET, networkInteractionListener,
                clazz, isResponseList, tag);
    }

    private <T> Request<T> post(String url, String body, Map<String, String> headers, final NetworkInteractionListener<T> networkInteractionListener,
                                Class<T> clazz, boolean isResponseList, Object tag) {
        return makeRequest(url, body, headers, Request.Method.GET, networkInteractionListener,
                clazz, isResponseList, tag);
    }


    private static <T> JsonRequest<T> makeRequest(String url, String body, final Map<String, String> headers, int requestType, final NetworkInteractionListener<T> networkInteractionListener,
                                                  final Class<T> clazz, final boolean isResponseList, Object tag) {
        Response.Listener<T> responseListener = new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                networkInteractionListener.onSuccess(response);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                networkInteractionListener.onFail(ResponseHelper.getNetworkErrorFromVolleyError(error));
            }
        };

        JsonRequest<T> request = new JsonRequest<T>(requestType, url, body, responseListener, errorListener) {
            @Override
            protected Response<T> parseNetworkResponse(NetworkResponse response) {
                return ResponseHelper.parseModel(response, getType(clazz, isResponseList));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        request.setTag(tag);
        return request;
    }

    private static <T> Type getType(Class<T> clazz, boolean isResponseList) {
        return isResponseList ? TypeToken.getParameterized(ArrayList.class, clazz).getType() : TypeToken.get(clazz).getType();
    }


}
