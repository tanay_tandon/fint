package com.example.tanaytandon.fint.internal;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

abstract class NetworkRequestWrapper {

    abstract String getURL();

    abstract Map<String, String> getHeaders();

    abstract String getBody();

    public <T> Request<T> get(final NetworkInteractionListener<T> networkInteractionListener,
                              Class<T> clazz, boolean isResponseList, Object tag) {
        return makeRequest(Request.Method.GET, networkInteractionListener, clazz, isResponseList, tag);
    }

    public <T> Request<T> post(final NetworkInteractionListener<T> networkInteractionListener,
                               Class<T> clazz, boolean isResponseList, Object tag) {
        return makeRequest(Request.Method.POST, networkInteractionListener, clazz, isResponseList, tag);
    }

    private <T> Type getType(Class<T> clazz, boolean isResponseList) {
        return isResponseList ? TypeToken.getParameterized(ArrayList.class, clazz).getType() : TypeToken.get(clazz).getType();
    }

    private <T> JsonRequest<T> makeRequest(int requestType, final NetworkInteractionListener<T> networkInteractionListener,
                                           final Class<T> clazz, final boolean isResponseList, Object tag) {
        Response.Listener<T> responseListener = new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                networkInteractionListener.onSuccess(response);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                networkInteractionListener.onFail(ResponseHelper.getNetworkErrorFromVolleyError(error));
            }
        };

        JsonRequest<T> request = new JsonRequest<T>(requestType, getURL(), getBody(), responseListener, errorListener) {
            @Override
            protected Response<T> parseNetworkResponse(NetworkResponse response) {
                return ResponseHelper.parseModel(response, getType(clazz, isResponseList));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return NetworkRequestWrapper.this.getHeaders();
            }
        };
        request.setTag(tag);
        return request;
    }
}
