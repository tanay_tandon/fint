package com.example.tanaytandon.fint;

import com.example.tanaytandon.fint.models.NetworkError;

/**
 * interface to get the result from Fint.
 */

public interface FintCallback<T> {

    void onSuccess(T result);

    void onFail(NetworkError networkError);

}
