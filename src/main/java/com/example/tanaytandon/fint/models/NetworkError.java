package com.example.tanaytandon.fint.models;

import android.os.Bundle;

public class NetworkError {

    private String message;

    private int statusCode;

    public String getMessage() {
        return message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    private NetworkError() {
    }

    public static NetworkError newInstance(String message, int statusCode) {
        NetworkError networkError = new NetworkError();
        networkError.message = message;
        networkError.statusCode = statusCode;
        return networkError;
    }
}
