package com.example.tanaytandon.fint.models;

/**
 * The company information is wrapped in {@link Company} and the price.
 */
public class CInfo {

    private Company company;

    private Double price;

    public Company getCompany() {
        return company;
    }

    public Double getPrice() {
        return price;
    }
}
