package com.example.tanaytandon.fint.models;

import com.google.gson.annotations.SerializedName;

public class Company {

    private String symbol;

    private String companyName;

    private String website;

    @SerializedName("CEO")
    private String ceo;

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return companyName;
    }

    public String getWebsite() {
        return website;
    }


    public String getCEO() {
        return ceo;
    }


}
